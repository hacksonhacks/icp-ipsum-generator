import urllib
import lxml.html
import re
connection = urllib.urlopen('http://www.azlyrics.com/i/icp.html')

dom =  lxml.html.fromstring(connection.read())

icpLinks = []
for miracle in dom.xpath('//a/@href'): 
    if "lyrics/icpinsaneclownposse" in miracle:
    	icpLinks.append(miracle)

# for eachLink in icpLinks:
# 	print eachLink

startLine = 0
endLine = 0
for lyricsPage in icpLinks:
	try:
		connection = urllib.urlopen('http://www.azlyrics.com/' + lyricsPage.replace("..",""))
		songPageLines = connection.readlines()
		for lineNumber,line in enumerate(songPageLines):
			if "<!-- start of lyrics -->" in line:
				startLine = lineNumber
			elif "<!-- end of lyrics -->" in line:
				endLine = lineNumber


		final = ''.join(songPageLines[startLine+2:endLine])
		final = re.sub('<[^<]+?>', '', final)
		with open("lyrics.txt", "a") as myfile:
			try:
				myfile.write(final)
		   	except:
		   		pass

   	except:
   		pass

print "done"