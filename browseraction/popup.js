window.onload = function() {
	document.getElementById("generate").onclick = function() {
		chrome.extension.sendMessage({
	        type: "generateParagraphs",
	        number: document.getElementById("paragraphs").value,
	    });
	}
}