
// listening for an event / one-time requests
// coming from the popup
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    switch(request.type) {
    	case "generateParagraphs":
    		generateParagraphs(request.number);
    	break;
    }
    return true;
});



// Sends messages to generate N number of paragraphs
var generateParagraphs = function(number) {
	chrome.tabs.getSelected(null, function(tab){
	    chrome.tabs.sendMessage(tab.id, {type: "generateParagraphs", number: number})
	});
}
