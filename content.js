//var app = angular.module()

function copyToClipboard (text) {
  window.prompt ("Copy to clipboard: Ctrl+C, Enter", text);
}

 // this is where file read function would go
 function makeTextTest(numberParagraphs) {

	var sentence = "The quick brown fox jumps over the lazy dog and saw a miracle. ";
	var paragraph = "";
	var finalSentence = "";

	for (var i=0;i<7;i++){
		paragraph += sentence;
	}

	for (var i=0;i<numberParagraphs;i++){
		finalSentence += paragraph;
	}

	return finalSentence;		
}

function test(){
	var data = load('tools/test.txt');
	alert(data);
}


// Generate text here.  Need to:
// 1. Read file
// 2. Read N number of paragraphs (paragraphs >= 12 words)
// 3. Put text into clipboard or into scroll box

chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {
	switch(message.type) {
		case "generateParagraphs":
		var text = makeTextTest(parseInt(message.number));
		copyToClipboard(text);
		break;
	}
});

